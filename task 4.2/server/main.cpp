#include <windows.h>
#include <iostream>
#include <clocale>

using namespace std;

int main()
{
	HANDLE hPipe;
	HANDLE hWriteEvent;
	OVERLAPPED olpWrite;

	char buffer[255];

	cout << "###Server application###\n";
	cout << "enter '!stop' to terminate program\n";

	// создаем именованый канал
	hPipe = CreateNamedPipe(R"(\\.\pipe\server-client-pipe)", PIPE_ACCESS_OUTBOUND | WRITE_DAC | FILE_FLAG_OVERLAPPED,
		PIPE_TYPE_MESSAGE | PIPE_WAIT, 1, 0, 0, NMPWAIT_USE_DEFAULT_WAIT, NULL);
	if (hPipe == INVALID_HANDLE_VALUE)
		cout << "Can't create pipe. Last error code: " << GetLastError() << endl;
	else 
		cout << "Pipe created" << endl;

	hWriteEvent = CreateEventA(NULL, TRUE, FALSE, "pipeEvent");
	if (hWriteEvent != INVALID_HANDLE_VALUE)
		cout << "Event created\n";
	else
		cout << "Can't create event. Last error code: " << GetLastError() << endl;

	cout << "Connecting... ";
	if (ConnectNamedPipe(hPipe, NULL))
		cout << "Connection established" << endl;
	else
		cout << endl << "Connection error" << endl;
	
	while (true)
	{
		cout << "Enter message: ";
		cin >> buffer;
		ZeroMemory(&olpWrite, sizeof(olpWrite));
		olpWrite.hEvent = hWriteEvent;

		// пишем собщение в канал
		WriteFile(hPipe, buffer, 255, NULL, &olpWrite);
		if (!strcmp(buffer, "!stop")) {
			break;
		}
		// успешно, только если дождались объекта
		if (WaitForSingleObject(hWriteEvent, INFINITE) == WAIT_OBJECT_0)
			cout << "Message sent\n";
		else 
			cout << "Error occurred\n";
	}

	// отключаемся от канала
	cout << "Server detach... ";
	if (DisconnectNamedPipe(hPipe))
		cout << "successful" << endl;
	else
		cout << "failed" << endl;

	// закрываем хэндл канала и события
	CloseHandle(hPipe);
	CloseHandle(hWriteEvent);

	system("pause");
	return 0;
}