#include <iostream>
#include <windows.h>
#include <clocale>

using namespace std;

char msg[255];
bool exitBool = false;

VOID CALLBACK FileIOCompletionRoutine(DWORD, DWORD, LPOVERLAPPED) {
	if (!strcmp(msg, "!stop"))
		exitBool = true;
	else
		cout << "check: " << msg << endl;
}

int main()
{
	cout << "###Client application###";
	HANDLE hOutPipe;
	int timeoutCur = 0, timeoutMax = 10;
	// пытамся поключиться 10 секунд раз в секунду
	while (timeoutCur < timeoutMax) {
		cout << "Attempting to connect " << timeoutCur << endl;
		hOutPipe = CreateFile(R"(\\.\pipe\server-client-pipe)", GENERIC_READ, 0, NULL, OPEN_ALWAYS, FILE_FLAG_OVERLAPPED, NULL);
		if (hOutPipe == INVALID_HANDLE_VALUE) {
			cout << "Can't connect, another try in 1 second" << endl;
		}
		else {
			cout << "Connection established successfully" << endl;
			break;
		}
		++timeoutCur;
		Sleep(1000);
	}

	if (timeoutCur == timeoutMax) {
		cout << "Terminated by timeout" << endl;
		system("pause");
		return -1;
	}

	// если вышло подключиться, то читаем сообщение
	while (true)
	{
		OVERLAPPED olpReadOverlapper;
		ZeroMemory(&olpReadOverlapper, sizeof(olpReadOverlapper));

		cout << "Waiting for message\n";

		// пишем в канал сообщение
		if (!ReadFileEx(hOutPipe, msg, 255, &olpReadOverlapper, FileIOCompletionRoutine)){
			cout << "Reading error" << endl;
			return 1;
		}
		SleepEx(INFINITE, 1);
		if (exitBool)
			break;
	}
	CloseHandle(hOutPipe);
	cout << "Connection closed\n";

	system("pause");
	return 0;
}