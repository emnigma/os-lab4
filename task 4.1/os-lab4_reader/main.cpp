#include <windows.h>
#include <fstream>
#include <time.h>
#include <stdlib.h>

using namespace std;

int main()
{
    // то же, что во writer'е, за исключением нескольких строчек
    const int MEMORY_PAGE_COUNT = 11;
    const int MEMORY_PAGE_SIZE = 4096;
    const char MemoryName[] = "shared_memory";
    const char WriterSemaphoreName[] = "Writer Semaphore";
    const char ReaderSemaphoreName[] = "Reader Semaphore";
    const char* MutexName[] = { "mutex0", "mutex1", "mutex2", "mutex3", "mutex4", "mutex5", "mutex6", "mutex7", "mutex8", "mutex9", "mutex10" };

    srand(time(NULL));
    fstream LogFile;
    LogFile.open("C:\\log.txt", fstream::out | fstream::app);

    HANDLE hReaderSemaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, ReaderSemaphoreName);
    if (!hReaderSemaphore)
        LogFile << "Reader: couldn't open reader semaphore\n";
    HANDLE hWriterSemaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, WriterSemaphoreName);
    if (!hWriterSemaphore)
        LogFile << "Writer: couldn't open writer semaphore\n";

    HANDLE hMemory = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, MemoryName);
    if (!hMemory)
        LogFile << "Reader: couldn't open file mapping\n";

    char* memory = (char*)MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (!memory)
        LogFile << "Reader: couldn't map view of file\n";
    VirtualLock((PVOID)memory, MEMORY_PAGE_COUNT*MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char));

    char data[MEMORY_PAGE_SIZE];
    int page = 0;

    for (int i = 0; i < 2; i++) {
        LogFile << GetTickCount() << " reader: wait semaphore \n";
        WaitForSingleObject(hWriterSemaphore, INFINITE);
        LogFile << GetTickCount() << " reader: ready \n";
        Sleep(10);
        int iter_start = rand() % MEMORY_PAGE_COUNT;
        for (int j = 0; j < MEMORY_PAGE_COUNT; j++) {
            // выбираем случайный начальный индекс, шагаем от него по всем страницам.
            // можно было бы просто подставить j, но так все страницы будут более-менее гарантировано заняты
            page = (iter_start + j) % MEMORY_PAGE_COUNT;
            if (memory[page] == 1)
                break;
            if (j == MEMORY_PAGE_COUNT - 1) {
                j = 0;
                //LogFile << GetTickCount() << " reader: non-empty page not found, waiting. \n";
                Sleep(300);
            }
        }

        int offset = page * MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char);
        HANDLE mutex = OpenMutexA(MUTEX_ALL_ACCESS, TRUE, MutexName[page]);
        WaitForSingleObject(mutex, INFINITE);
        if (mutex != NULL) {
            Sleep(10);
            if (memory[page] == 0) {
                // если сейчас страница была прочтена
                LogFile << GetTickCount() << " reader: page collision! page = " << page << "\n";
                i++;
                ReleaseMutex(mutex);
                ReleaseSemaphore(hWriterSemaphore, 1, NULL);
            }
            else {
                // ставим статус "чтение"
                memory[page] = 0;
                LogFile << GetTickCount() << " reader: start reading\n";

                // копруем память в переменную data
                memcpy(data, memory + offset, MEMORY_PAGE_SIZE);
                LogFile << GetTickCount() << " reader: reading completed. page " << page << " : " << data << ". waiting\n";
                ReleaseMutex(mutex);
                ReleaseSemaphore(hReaderSemaphore, 1, NULL);
            }
        }
        Sleep(200 + rand() % 500);
    }
    return 0;
}
