#include <windows.h>
#include <iostream>

using namespace std;

int main()
{
    // больше или равное количеству страниц
    const int READER_COUNT = 12;
    const int WRITER_COUNT = 12;

    // максимальное количество запросов на ресурсы одновременно
    const int SEMAPHORE_MAX_VALUE = 11;
    // количество страниц = (8) 3+0+7+1+0 = 11
    const int MEMORY_PAGE_COUNT = 11;
    // размер страницы в байтах
    const int MEMORY_PAGE_SIZE = 4096;

    const char MemoryName[] = "shared_memory";

    const char WriterSemaphoreName[] = "Writer semaphore";
    const char ReaderSemaphoreName[] = "Reader semaphore";
    const char* MutexName[] = { "mutex0", "mutex1", "mutex2", "mutex3", "mutex4", "mutex5", "mutex6", "mutex7", "mutex8", "mutex9", "mutex10" };

    LPCSTR writerProgramPath = "C:/Users/nigma/Desktop/os-lab4/task 4.1/os-lab4_reader/cmake-build-debug/os_lab4_reader.exe";
    LPCSTR readerProgramPath = "C:/Users/nigma/Desktop/os-lab4/task 4.1/os-lab4_writer/cmake-build-debug/os_lab4_writer.exe";
    LPCSTR logPath = "C:\\log.txt";

    PROCESS_INFORMATION piWriterProcessInfo[WRITER_COUNT];
    PROCESS_INFORMATION piReaderProcessInfo[READER_COUNT];

    // создан семафор для писателя
    HANDLE hWriterSemaphore = CreateSemaphoreA(NULL, SEMAPHORE_MAX_VALUE, SEMAPHORE_MAX_VALUE, WriterSemaphoreName);
    if (!hWriterSemaphore)
        cout << "Couldn't create writer semaphore\n";

    // создан семафор для читателя
    HANDLE hReaderSemaphore = CreateSemaphoreA(NULL, SEMAPHORE_MAX_VALUE, SEMAPHORE_MAX_VALUE, ReaderSemaphoreName);
    if (!hReaderSemaphore)
        cout << "Couldn't create reader semaphore\n";

    // создаем N мьютексов по количеству страниц памяти, чтобы запретить доступ к занятой странице
    HANDLE phMemoryPageMutex[MEMORY_PAGE_COUNT];
    for (int i = 0; i < MEMORY_PAGE_COUNT; i++)
    {
        phMemoryPageMutex[i] = CreateMutexA(NULL, false, MutexName[i]);
        if (!phMemoryPageMutex[i])
            cout << "Couldn't create " << i << " mutex\n";
    }

    // создаем общий файл
    HANDLE hFile = CreateFileA("C:\\sharefile.txt",
                               GENERIC_READ | GENERIC_WRITE,
                               FILE_SHARE_READ | FILE_SHARE_WRITE,
                               NULL,
                               CREATE_NEW,
                               FILE_ATTRIBUTE_NORMAL,
                               NULL);
    if (hFile)
        cout << "Manager: C:/sharefile.txt created\n";

    // создаем маппинг для общего файла
    HANDLE hMemory = CreateFileMappingA(hFile, NULL, PAGE_READWRITE, 0,
                                        MEMORY_PAGE_COUNT*MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char), MemoryName);
    if (!hMemory)
        cout << "Manager: couldn't create file mapping. error code: " << GetLastError() << endl;

    // выделяем память общему файлу
    char* memory = (char*)MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (!memory)
        cout << "Manager: couldn't map view of file\n";

    // обнуляем память
    ZeroMemory(memory, MEMORY_PAGE_COUNT*MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char));

    // создаем процессы читателей и писателей
    for (int i = 0; i < WRITER_COUNT; i++) {
        STARTUPINFOA startinfo = { sizeof(startinfo) };
        if (!CreateProcessA(writerProgramPath, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &startinfo, &(piWriterProcessInfo[i])))
            cout << "Manager: couldn't create writer process #" << i << " error code: " << GetLastError() << endl;
        else
            cout << "Writer " << i << " created\n";
        Sleep(100);
    }
    for (int i = 0; i < READER_COUNT; i++) {
        STARTUPINFOA startinfo = { sizeof(startinfo) };
        if (!CreateProcessA(readerProgramPath, NULL, NULL, NULL, TRUE, 0, NULL, NULL, &startinfo, &(piWriterProcessInfo[i])))
            cout << "Manager: couldn't create reader process #" << i << " error code: " << GetLastError() << "\n";
        else
            cout << "Reader " << i << " created\n";
        Sleep(100);
    }

    for (int i = 0; i < WRITER_COUNT; i++) {
        cout << "Manager: waiting writer #" << i << "\n";
        WaitForSingleObject(piWriterProcessInfo[i].hProcess, 2000);
    }
    for (int i = 0; i < READER_COUNT; i++) {
        cout << "Manager: waiting reader #" << i << "\n";
        WaitForSingleObject(piReaderProcessInfo[i].hProcess, 2000);
    }
    cout << "Program finished\n";
    //system("pause");
    return 0;
}