#include <windows.h>
#include <fstream>
#include <time.h>
#include <stdlib.h>
#include <iostream>

using namespace std;

int main()
{
    const int MEMORY_PAGE_COUNT = 11;
    const int MEMORY_PAGE_SIZE = 4096;

    // имена общих системных ресурсов
    const char memoryName[] = "shared_memory";
    const char WriterSemaphoreName[] = "Writer Semaphore";
    const char ReaderSemaphoreName[] = "Reader Semaphore";
    const char* MutexName[] = { "mutex0", "mutex1", "mutex2", "mutex3", "mutex4", "mutex5", "mutex6", "mutex7", "mutex8", "mutex9", "mutex10" };

    srand(time(NULL));
    // анонимный канал для логгирования

    fstream LogFile;
    LogFile.open("C:\\log.txt", fstream::out | fstream::app);

    // открываем именованые семафоры читателя и писателя
    HANDLE hReaderSemaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, ReaderSemaphoreName);
    if (!hReaderSemaphore)
        LogFile << "Reader: couldn't open reader semaphore\n";
    HANDLE hWriterSemaphore = OpenSemaphoreA(SEMAPHORE_ALL_ACCESS, FALSE, WriterSemaphoreName);
    if (!hWriterSemaphore)
        LogFile << "Writer: couldn't open writer semaphore\n";

    // открываем маппинг общего файла
    HANDLE hMemory = OpenFileMappingA(FILE_MAP_ALL_ACCESS, FALSE, memoryName);
    if (!hMemory)
        LogFile << "Writer: couldn't open file mapping\n";

    char* memory = (char*)MapViewOfFile(hMemory, FILE_MAP_ALL_ACCESS, 0, 0, 0);
    if (memory)
        LogFile << "Writer: couldn't write data to memory\n";

    // шоб случайно не выгрузил страницу из памяти
    VirtualLock((PVOID)memory, MEMORY_PAGE_COUNT*MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char));

    char data[MEMORY_PAGE_SIZE];
    int page = 0;

    // почему дважды?
    for (int i = 0; i < 2; i++) {
        LogFile << GetTickCount() << " writer: wait semaphore \n";

        // подождать и захватить семафор читателя = уменьшить число чистых страниц
        // по сути - ждем, пока освободится квота на ресурсы
        WaitForSingleObject(hReaderSemaphore, INFINITE);
        LogFile << GetTickCount() << " writer: ready \n";
        Sleep(10);
        int iter_start = rand() % MEMORY_PAGE_COUNT;
        for (int j = 0; j < MEMORY_PAGE_COUNT; j++) {
            // выбираем случайный начальный индекс, шагаем от него по всем страницам.
            // можно было бы просто подставить j, но так все страницы будут более-менее гарантировано заняты
             page = (iter_start + j) % MEMORY_PAGE_COUNT;
            // если нашли пустую страницу
            if (memory[page] == 0)
                break;
            // если не нашли пустую страницу - ждем, пока какая-нибудь страница не станет пустой
            if (j == MEMORY_PAGE_COUNT - 1) {
                j = 0;
                LogFile << GetTickCount() << " writer: empty page not found, waiting. \n";
                Sleep(300);
            }
        }
        // пишем в переменную "data" текущий статус страницы
        sprintf(data, "this is page #%d. writing time = %d", page, GetTickCount());

        // смещение относительно начала файла
        int offset = page * MEMORY_PAGE_SIZE + MEMORY_PAGE_COUNT * sizeof(char);
        // захватываем мьютекс текущей страницы и проверяем его доступность
        HANDLE mutex = OpenMutexA(MUTEX_ALL_ACCESS, TRUE, MutexName[page]);
        WaitForSingleObject(mutex, INFINITE);
        if (mutex) {
            Sleep(10);
            if (memory[page] == 1) {
                // если уже происходит запись (не должно происходить)
                LogFile << GetTickCount() << " writer: page collision! page = " << page << "\n";
                // завершаем цикл
                i++;
                ReleaseMutex(mutex);
                ReleaseSemaphore(hReaderSemaphore, 1, NULL);
            }
            else {
                // статус "запись"
                memory[page] = 1;
                LogFile << GetTickCount() << " writer: start writing\n";

                // копируем переменную data в память
                memcpy(memory + offset, data, MEMORY_PAGE_SIZE);
                LogFile << GetTickCount() << " writer: writing completed. page " << page << ". waiting\n";

                // отпускаем объекты синхронизации
                ReleaseMutex(mutex);
                ReleaseSemaphore(hWriterSemaphore, 1, NULL);
            }
        }
        Sleep(200 + rand() % 500);
    }
    LogFile.close();
    return 0;
}